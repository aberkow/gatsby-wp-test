const _ = require("lodash")
const Promise = require("bluebird")
const path = require("path")
const select = require(`unist-util-select`)
const fs = require(`fs-extra`)

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators

  return new Promise((resolve, reject) => {
    const pages = []
    const blogPost = path.resolve("./src/templates/blog-post.js")
    resolve(
      graphql(
        `
        {
          allWordpressPost {
            edges {
              node {
                content
                date
                slug
                title
              }
            }
          }
        }
    `
      ).then(result => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }

        // Create blog posts pages.
        const test = _.each(result.data.allWordpressPost.edges, edge => {
          createPage({
            path: `/${edge.node.slug}`,
            component: blogPost,
            context: {
              // content: edge.node.content,
              slug: edge.node.slug
            },
          })
        })
        // console.log(test);
        return test;
      })
    )
  })
}
