import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'

import Bio from '../components/Bio'

class BlogIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const posts = get(this, 'props.data.allWordpressPost.edges')
    return (
      <div>
        <Helmet title={get(this, 'props.data.site.siteMetadata.title')} />
        {posts.map(post => {
          if (post.node.slug !== '/404/') {
            const title = get(post, 'node.title') || post.node.slug
            return (
              <div key={post.node.slug}>
                <h3>
                  <Link to={post.node.slug} >
                    {post.node.title}
                  </Link>
                </h3>
                <small>{post.node.date}</small>
                <p dangerouslySetInnerHTML={{ __html: post.node.excerpt }} />
              </div>
            )
          }
        })}
      </div>
    )
  }
}

BlogIndex.propTypes = {
  route: React.PropTypes.object,
}

export default BlogIndex

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    allWordpressPost {
      edges {
        node {
          excerpt
          slug
          date(formatString: "DD MMMM, YYYY")
          title
        }
      }
    }
  }
`
