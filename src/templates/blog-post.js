import React from 'react'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'
import get from 'lodash/get'

class BlogPostTemplate extends React.Component {
  render() {
    const post = this.props.data.wordpressPost
    const siteTitle = get(this.props, 'data.site.siteMetadata.title')
    console.log(this.props, 'post');
    return (
      <div>
        <Helmet title={`${post.title} | ${siteTitle}`} />
        <h1>{post.title}</h1>
        <p>
          {post.date}
        </p>
        <div dangerouslySetInnerHTML={{ __html: post.content }} />
      </div>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query WordpressPostBySlug($slug: String!) {
    wordpressPost( slug: { eq: $slug } ) {
      content
      date
      title
      slug
    }
  }
`
